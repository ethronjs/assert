#imports
from "ethron" use suite, test
from "chai" use assert
use "../../../../@ethronjs/assert" as ja

#Test.
export suite(__filename, proc()
  test("isBool()", proc(params)
    const val = ja(true)
    assert.strictEqual(bind(val, params.meth)(), val)
  end).forEach(
    {sub="isBool()", meth="isBool"}
    {sub="isBoolean", meth="isBoolean"}
  )

  test("isNotBool() - error", proc(params)
    const val = ja(true)
    assert.throws(proc() bind(val, params.meth)() end)
  end).forEach(
    {sub="isNotBool()", meth="isNotBool"}
    {sub="isNotBoolean()", meth="isNotBoolean"}
  )

  test("isText() - error", proc()
    const val = ja(true)
    assert.throws(proc() val.isText() end)
  end)

  test("isNotText()", proc()
    const val = ja(true)
    assert.strictEqual(val.isNotText(), val)
  end)

  test("isNum() - error", proc()
    const val = ja(true)
    assert.throws(proc() val.isNum() end)
  end)

  test("isNotNum()", proc()
    const val = ja(true)
    assert.strictEqual(val.isNotNum(), val)
  end)

  test("isList() - error", proc()
    const val = ja(true)
    assert.throws(proc() val.isList() end)
  end)

  test("isNotList()", proc()
    const val = ja(true)
    assert.strictEqual(val.isNotList(), val)
  end)

  test("isSet() - error", proc()
    const val = ja(true)
    assert.throws(proc() val.isSet() end)
  end)

  test("isNotSet()", proc()
    const val = ja(true)
    assert.strictEqual(val.isNotSet(), val)
  end)

  test("isMap() - error", proc()
    const val = ja(true)
    assert.throws(proc() val.isMap() end)
  end)

  test("isNotMap()", proc()
    const val = ja(true)
    assert.strictEqual(val.isNotMap(), val)
  end)

  test("isFn() - error", proc()
    const val = ja(true)
    assert.throws(proc() val.isFn() end)
  end)

  test("isNotFn()", proc()
    const val = ja(true)
    assert.strictEqual(val.isNotFn(), val)
  end)

  test("isCallable() - error", proc()
    const val = ja(true)
    assert.throws(proc() val.isCallable() end)
  end)

  test("isNotCallable()", proc()
    const val = ja(true)
    assert.strictEqual(val.isNotCallable(), val)
  end)

  suite("eq()", proc()
    test("eq() - true equal to true", proc()
      const val = ja(true)
      assert.strictEqual(val.eq(true), val)
    end)

    test("eq() - error when true not equal to false", proc()
      const val = ja(true)
      assert.throws(proc() val.eq(false) end)
    end)

    test("eq() - error when text", proc()
      const val = ja(true)
      assert.throws(proc() val.eq("true") end)
    end)

    test("eq() - error when num", proc()
      const val = ja(false)
      assert.throws(proc() val.eq([]) end)
    end)

    test("eq() - error when map", proc()
      const val = ja(false)
      assert.throws(proc() val.eq({}) end)
    end)

    test("eq() - error when fn", proc()
      const val = ja(false)
      assert.throws(proc() val.eq(nop) end)
    end)
  end)

  suite("ne()", proc()
    test("ne() - true not equal to false", proc()
      const val = ja(true)
      assert.strictEqual(val.ne(false), val)
    end)

    test("ne() - error when true not equal to true", proc()
      const val = ja(true)
      assert.throws(proc() val.ne(true) end)
    end)

    test("ne() - when text", proc()
      const val = ja(true)
      assert.strictEqual(val.ne("true"), val)
    end)

    test("ne() - when num", proc()
      const val = ja(false)
      assert.strictEqual(val.ne([]), val)
    end)

    test("ne() - when map", proc()
      const val = ja(false)
      assert.strictEqual(val.ne({}), val)
    end)

    test("ne() - when fn", proc()
      const val = ja(false)
      assert.strictEqual(val.ne(nop), val)
    end)
  end)

  suite("sameAs()", proc()
    const val = ja(true)

    test("sameAs()", proc()
      assert.strictEqual(val.sameAs(true), val)
    end)

    test("sameAs() - error with bool", proc()
      assert.throws(proc() val.sameAs(false) end)
    end)

    test("sameAs() - error with text", proc()
      assert.throws(proc() val.sameAs("true") end)
    end)

    test("sameAs() - error with num", proc()
      assert.throws(proc() val.sameAs(1) end)
    end)

    test("sameAs() - error with list", proc()
      assert.throws(proc() val.sameAs([1, 2, 3]) end)
    end)

    test("sameAs() - error with map", proc()
      assert.throws(proc() val.sameAs({x = 1}) end)
    end)

    test("sameAs() - error with fn", proc()
      assert.throws(proc() val.sameAs(nop) end)
    end)
  end)

  suite("notSameAs()", proc()
    const val = ja(true)

    test("notSameAs()", proc()
      assert.strictEqual(val.notSameAs(false), val)
    end)

    test("notSameAs() - error with bool", proc()
      assert.throws(proc() val.notSameAs(true) end)
    end)

    test("notSameAs() - with text", proc()
      assert.strictEqual(val.notSameAs("true"), val)
    end)

    test("notSameAs() - with num", proc()
      assert.strictEqual(val.notSameAs(1), val)
    end)

    test("notSameAs() - with list", proc()
      assert.strictEqual(val.notSameAs([1, 2, 3]), val)
    end)

    test("notSameAs() - with map", proc()
      assert.strictEqual(val.notSameAs({x = 1}), val)
    end)

    test("notSameAs() - with fn", proc()
      assert.strictEqual(val.notSameAs(nop), val)
    end)
  end)

  suite("lt()", proc()
    const val = ja(true)

    test("lt(bool) - error", proc()
      assert.throws(proc() val.lt(false) end)
    end)

    test("lt(text) - error", proc()
      assert.throws(proc() val.lt("true") end)
    end)

    test("lt(num) - error", proc()
      assert.throws(proc() val.lt(1) end)
    end)

    test("lt(nil) - error", proc()
      assert.throws(proc() val.lt(nil) end)
    end)

    test("lt(list) - error", proc()
      assert.throws(proc() val.lt([]) end)
    end)

    test("lt(map) - error", proc()
      assert.throws(proc() val.lt({}) end)
    end)

    test("lt(fn) - error", proc()
      assert.throws(proc() val.lt(nop) end)
    end)
  end)

  suite("le()", proc()
    const val = ja(true)

    test("le(bool)", proc()
      assert.throws(proc() val.le(false)end)
    end)

    test("le(text) - error", proc()
      assert.throws(proc() val.le("true") end)
    end)

    test("le(num) - error", proc()
      assert.throws(proc() val.le(1) end)
    end)

    test("le(nil) - error", proc()
      assert.throws(proc() val.le(nil) end)
    end)

    test("le(list) - error", proc()
      assert.throws(proc() val.le([]) end)
    end)

    test("le(map) - error", proc()
      assert.throws(proc() val.le({}) end)
    end)

    test("le(fn) - error", proc()
      assert.throws(proc() val.le(nop) end)
    end)
  end)

  suite("gt()", proc()
    const val = ja(true)

    test("gt(bool) - error", proc()
      assert.throws(proc() val.gt(false) end)
    end)

    test("gt(text) - error", proc()
      assert.throws(proc() val.gt("true") end)
    end)

    test("gt(num) - error", proc()
      assert.throws(proc() val.gt(1) end)
    end)

    test("gt(nil) - error", proc()
      assert.throws(proc() val.gt(nil) end)
    end)

    test("gt(list) - error", proc()
      assert.throws(proc() val.gt([]) end)
    end)

    test("gt(map) - error", proc()
      assert.throws(proc() val.gt({}) end)
    end)

    test("gt(fn) - error", proc()
      assert.throws(proc() val.gt(nop) end)
    end)
  end)

  suite("ge()", proc()
    const val = ja(true)

    test("ge(bool) - error", proc()
      assert.throws(proc() val.ge(false) end)
    end)

    test("ge(text) - error", proc()
      assert.throws(proc() val.ge("true") end)
    end)

    test("ge(num) - error", proc()
      assert.throws(proc() val.ge(1) end)
    end)

    test("ge(nil) - error", proc()
      assert.throws(proc() val.ge(nil) end)
    end)

    test("ge(list) - error", proc()
      assert.throws(proc() val.ge([]) end)
    end)

    test("ge(map) - error", proc()
      assert.throws(proc() val.ge({}) end)
    end)

    test("ge(fn) - error", proc()
      assert.throws(proc() val.ge(nop) end)
    end)
  end)

  suite("between()", proc()
    const val = ja(true)

    test("between(bool, bool) - error", proc()
      assert.throws(proc() val.between(true, false) end)
    end)

    test("between(text, text) - error", proc()
      assert.throws(proc() val.between("true", "false") end)
    end)

    test("between(num, num) - error", proc()
      assert.throws(proc() val.between(0, 1) end)
    end)

    test("between(list, list) - error", proc()
      assert.throws(proc() val.between([], [true, false]) end)
    end)

    test("between(map, map) - error", proc()
      assert.throws(proc() val.between({}, {x = 1, y = 2}) end)
    end)

    test("between(nil, nil) - error", proc()
      assert.throws(proc() val.between(nil, nil) end)
    end)

    test("between(fn, fn) - error", proc()
      assert.throws(proc() val.between(nop, nop) end)
    end)
  end)

  suite("notBetween()", proc()
    const val = ja(true)

    test("notBetween(bool, bool)", proc()
      assert.strictEqual(val.notBetween(true, false), val)
    end)

    test("notBetween(text, text)", proc()
      assert.strictEqual(val.notBetween("true", "false"), val)
    end)

    test("notBetween(num, num)", proc()
      assert.strictEqual(val.notBetween(0, 1), val)
    end)

    test("notBetween(list, list)", proc()
      assert.strictEqual(val.notBetween([], [true, false]), val)
    end)

    test("notBetween(map, map)", proc()
      assert.strictEqual(val.notBetween({}, {x = 1, y = 2}), val)
    end)

    test("notBetween(nil, nil)", proc()
      assert.strictEqual(val.notBetween(nil, nil), val)
    end)

    test("notBetween(fn, fn)", proc()
      assert.strictEqual(val.notBetween(nop, nop), val)
    end)
  end)

  suite("includes()", proc()
    const val = ja(true)

    test("includes(bool) - error", proc()
      assert.throws(proc() val.includes(true) end)
    end)

    test("includes(text) - error", proc()
      assert.throws(proc() val.includes("true") end)
    end)

    test("includes(num) - error", proc()
      assert.throws(proc() val.includes(123) end)
    end)

    test("includes(list) - error", proc()
      assert.throws(proc() val.includes([true]) end)
    end)

    test("includes(map) - error", proc()
      assert.throws(proc() val.includes({x = true}) end)
    end)

    test("includes(nil) - error", proc()
      assert.throws(proc() val.includes(nil) end)
    end)

    test("includes(fn) - error", proc()
      assert.throws(proc() val.includes(nop) end)
    end)
  end)

  suite("notIncludes()", proc()
    const val = ja(true)

    test("notIncludes(bool)", proc()
      assert.strictEqual(val.notIncludes(true), val)
    end)

    test("notIncludes(text)", proc()
      assert.strictEqual(val.notIncludes("true"), val)
    end)

    test("notIncludes(num)", proc()
      assert.strictEqual(val.notIncludes(123), val)
    end)

    test("notIncludes(list)", proc()
      assert.strictEqual(val.notIncludes([true]), val)
    end)

    test("notIncludes(map)", proc()
      assert.strictEqual(val.notIncludes({x = true}), val)
    end)

    test("notIncludes(nil)", proc()
      assert.strictEqual(val.notIncludes(nil), val)
    end)

    test("notIncludes(fn)", proc()
      assert.strictEqual(val.notIncludes(nop) , val)
    end)
  end)

  suite("has()", proc()
    const val = ja(true)

    test("has(mem) - error", proc()
      assert.throws(proc() val.has("xyz") end)
    end)

    test("has(mems) - error", proc()
      assert.throws(proc() val.has(["abc", "xyz"]) end)
    end)
  end)

  suite("notHas()", proc()
    const val = ja(true)

    test("notHas(mem)", proc()
      assert.strictEqual(val.notHas("xyz"), val)
    end)

    test("notHas(mems)", proc()
      assert.strictEqual(val.notHas(["abc", "xyz"]), val)
    end)
  end)

  test("isEmpty() - error", proc()
    const val = ja(true)
    assert.throws(proc() val.isEmpty() end)
  end)

  test("isNotEmpty() - error", proc()
    const val = ja(true)
    assert.throws(proc() val.isNotEmpty() end)
  end)

  test("len() - error", proc()
    const val = ja(true)
    assert.throws(proc() val.len(0) end)
  end)

  test("notLen() - error", proc()
    const val = ja(true)
    assert.throws(proc() val.notLen(0) end)
  end)
end)
