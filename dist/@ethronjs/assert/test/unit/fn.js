"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

var _chai = require("chai");

const ja = _core.dogma.use(require("../../../../@ethronjs/assert"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    (0, _ethron.test)("isFn()", params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, null);

      {
        const val = ja(_core.dogma.nop());

        _chai.assert.strictEqual((0, _core.bind)(val, params.meth)(), val);
      }
    }).forEach({
      ["sub"]: "isFn()",
      ["meth"]: "isFn"
    }, {
      ["sub"]: "isFunction()",
      ["meth"]: "isFunction"
    });
    (0, _ethron.test)("isNotFn()", params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, null);

      {
        const val = ja(_core.dogma.nop());

        _chai.assert.throws(() => {
          {
            (0, _core.bind)(val, params.meth)();
          }
        });
      }
    }).forEach({
      ["sub"]: "isNotFn()",
      ["meth"]: "isNotFn"
    }, {
      ["sub"]: "isNotFunction()",
      ["meth"]: "isNotFunction"
    });
    (0, _ethron.test)("isCallable()", () => {
      {
        const val = ja(_core.dogma.nop());

        _chai.assert.strictEqual(val.isCallable(), val);
      }
    });
    (0, _ethron.test)("isNotCallable()", () => {
      {
        const val = ja(_core.dogma.nop());

        _chai.assert.throws(() => {
          {
            val.isNotCallable();
          }
        });
      }
    });
    (0, _ethron.suite)("raises()", () => {
      {
        (0, _ethron.test)("raises()", () => {
          {
            const val = ja(() => {
              {
                _core.dogma.raise("an error.");
              }
            });

            _chai.assert.strictEqual(val.raises(), val);
          }
        });
        (0, _ethron.test)("raises() - error when must raise error", () => {
          {
            const val = ja(_core.dogma.nop());

            _chai.assert.throws(() => {
              {
                val.raises();
              }
            });
          }
        });
        (0, _ethron.test)("raises() - error when error raised but must be other error", () => {
          {
            const val = ja(() => {
              {
                _core.dogma.raise("my error msg.");
              }
            });

            _chai.assert.throws(() => {
              {
                val.raises("other error msg.");
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("notRaises()", () => {
      {
        (0, _ethron.test)("doesNotRaise()", () => {
          {
            const val = ja(_core.dogma.nop());

            _chai.assert.strictEqual(val.doesNotRaise(), val);
          }
        });
        (0, _ethron.test)("notRaises()", () => {
          {
            const val = ja(_core.dogma.nop());

            _chai.assert.strictEqual(val.notRaises(), val);
          }
        });
        (0, _ethron.test)("notRaises() - error when some error raised", () => {
          {
            const val = ja(() => {
              {
                _core.dogma.raise("an error.");
              }
            });

            _chai.assert.throws(() => {
              {
                val.notRaises();
              }
            });
          }
        });
        (0, _ethron.test)("notRaises() - error when raised error with specified error", () => {
          {
            const val = ja(() => {
              {
                _core.dogma.raise("an error.");
              }
            });

            _chai.assert.throws(() => {
              {
                val.notRaises("an error.");
              }
            });
          }
        });
        (0, _ethron.test)("notRaises() - when raised error but being another", () => {
          {
            const val = ja(() => {
              {
                _core.dogma.raise("an error.");
              }
            });

            _chai.assert.strictEqual(val.notRaises("other error."), val);
          }
        });
      }
    });
  }
});