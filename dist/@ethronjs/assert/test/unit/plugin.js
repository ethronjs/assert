"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

var _chai = require("chai");

const ja = _core.dogma.use(require("../../../../@ethronjs/assert"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    ja.plugin({
      ["file"]: _core.dogma.nop(),
      ["dir"]: _core.dogma.nop()
    });

    _chai.assert.isFunction(ja.file);

    _chai.assert.isFunction(ja.dir);
  }
});