"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

var _chai = require("chai");

const ja = _core.dogma.use(require("../../../../@ethronjs/assert"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    (0, _ethron.suite)("isXxx()", () => {
      {
        const val = ja((0, _core.set)(1, 2, 3));
        (0, _ethron.test)("isSet()", () => {
          {
            _chai.assert.strictEqual(val.isSet(), val);
          }
        });
        (0, _ethron.test)("isList()", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isList();
              }
            });
          }
        });
        (0, _ethron.test)("isBool() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isBool();
              }
            });
          }
        });
        (0, _ethron.test)("isNum() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isNum();
              }
            });
          }
        });
        (0, _ethron.test)("isText() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isText();
              }
            });
          }
        });
        (0, _ethron.test)("isMap()", () => {
          {
            _chai.assert.strictEqual(val.isMap(), val);
          }
        });
        (0, _ethron.test)("isNil() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isNil();
              }
            });
          }
        });
        (0, _ethron.test)("isFn() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isFn();
              }
            });
          }
        });
        (0, _ethron.test)("isCallable() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isCallable();
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("isNotXxx()", () => {
      {
        const val = ja((0, _core.set)(1, 2, 3));
        (0, _ethron.test)("isNotSet() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isNotSet();
              }
            });
          }
        });
        (0, _ethron.test)("isNotList()", () => {
          {
            _chai.assert.strictEqual(val.isNotList(), val);
          }
        });
        (0, _ethron.test)("isBool()", () => {
          {
            _chai.assert.strictEqual(val.isNotBool(), val);
          }
        });
        (0, _ethron.test)("isNum()", () => {
          {
            _chai.assert.strictEqual(val.isNotNum(), val);
          }
        });
        (0, _ethron.test)("isText()", () => {
          {
            _chai.assert.strictEqual(val.isNotText(), val);
          }
        });
        (0, _ethron.test)("isMap() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isNotMap();
              }
            });
          }
        });
        (0, _ethron.test)("isNil()", () => {
          {
            _chai.assert.strictEqual(val.isNotNil(), val);
          }
        });
        (0, _ethron.test)("isFn()", () => {
          {
            _chai.assert.strictEqual(val.isNotFn(), val);
          }
        });
        (0, _ethron.test)("isCallable()", () => {
          {
            _chai.assert.strictEqual(val.isNotCallable(), val);
          }
        });
      }
    });
  }
});