"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

var _chai = require("chai");

const ja = _core.dogma.use(require("../../../../@ethronjs/assert"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    (0, _ethron.test)("isText()", () => {
      {
        const val = ja("ciao mondo1");

        _chai.assert.strictEqual(val.isText(), val);
      }
    });
    (0, _ethron.test)("isNotText()", () => {
      {
        _chai.assert.throws(() => {
          {
            ja("ciao mondo!").isNotText();
          }
        });
      }
    });
    (0, _ethron.test)("isBool()", () => {
      {
        _chai.assert.throws(() => {
          {
            ja("true").isBool();
          }
        });
      }
    });
    (0, _ethron.test)("isNotBool()", () => {
      {
        const val = ja("true");

        _chai.assert.strictEqual(val.isNotBool(), val);
      }
    });
    (0, _ethron.test)("raises()", () => {
      {
        const val = ja("bonjour");

        _chai.assert.strictEqual(val.raises(), val);
      }
    });
    (0, _ethron.test)("notRaises()", () => {
      {
        const val = ja("buongiorno");

        _chai.assert.throws(() => {
          {
            val.notRaises();
          }
        });
      }
    });
    (0, _ethron.test)("isNil()", () => {
      {
        const val = ja("ciao mondo!");

        _chai.assert.throws(() => {
          {
            val.isNil();
          }
        });
      }
    });
    (0, _ethron.test)("isNotNil()", () => {
      {
        const val = ja("Bonjour monde!");

        _chai.assert.strictEqual(val.isNotNil(), val);
      }
    });
    (0, _ethron.test)("isNum()", () => {
      {
        const val = ja("123");

        _chai.assert.throws(() => {
          {
            val.isNum();
          }
        });
      }
    });
    (0, _ethron.test)("isNotNum()", () => {
      {
        const val = ja("123");

        _chai.assert.strictEqual(val.isNotNum(), val);
      }
    });
    (0, _ethron.test)("isList()", () => {
      {
        const val = ja("[1, 2, 3]");

        _chai.assert.throws(() => {
          {
            val.isList();
          }
        });
      }
    });
    (0, _ethron.test)("isNotList()", () => {
      {
        const val = ja("[1, 2, 3]");

        _chai.assert.strictEqual(val.isNotList(), val);
      }
    });
    (0, _ethron.test)("isMap()", () => {
      {
        const val = ja("{x = 1, y = 2}");

        _chai.assert.throws(() => {
          {
            val.isMap();
          }
        });
      }
    });
    (0, _ethron.test)("isNotMap()", () => {
      {
        const val = ja("{x = 1, y = 2}");

        _chai.assert.strictEqual(val.isNotMap(), val);
      }
    });
    (0, _ethron.test)("isFn()", () => {
      {
        const val = ja("fn() end");

        _chai.assert.throws(() => {
          {
            val.isFn();
          }
        });
      }
    });
    (0, _ethron.test)("isNotFn()", () => {
      {
        const val = ja("fn() end");

        _chai.assert.strictEqual(val.isNotFn(), val);
      }
    });
    (0, _ethron.test)("isCallable()", () => {
      {
        const val = ja("call()");

        _chai.assert.throws(() => {
          {
            val.isCallable();
          }
        });
      }
    });
    (0, _ethron.test)("isNotCallable()", () => {
      {
        const val = ja("call()");

        _chai.assert.strictEqual(val.isNotCallable(), val);
      }
    });
    (0, _ethron.suite)("eq()", () => {
      {
        (0, _ethron.test)("eq(text)", () => {
          {
            const val = ja("bonjour");

            _chai.assert.strictEqual(val.eq("bonjour"), val);
          }
        });
        (0, _ethron.test)("eq(text) - error", () => {
          {
            const val = ja("bonjour");

            _chai.assert.throws(() => {
              {
                val.eq("ciao");
              }
            });
          }
        });
        (0, _ethron.test)("eq(bool) - error", () => {
          {
            const val = ja("true");

            _chai.assert.throws(() => {
              {
                val.eq(true);
              }
            });
          }
        });
        (0, _ethron.test)("eq(num) - error", () => {
          {
            const val = ja("123");

            _chai.assert.throws(() => {
              {
                val.eq(123);
              }
            });
          }
        });
        (0, _ethron.test)("eq(list) - error", () => {
          {
            const val = ja("[1, 2, 3]");

            _chai.assert.throws(() => {
              {
                val.eq([1, 2, 3]);
              }
            });
          }
        });
        (0, _ethron.test)("eq(map) - error", () => {
          {
            const val = ja("{x = 1, y = 2}");

            _chai.assert.throws(() => {
              {
                val.eq({
                  ["x"]: 1,
                  ["y"]: 2
                });
              }
            });
          }
        });
        (0, _ethron.test)("eq(fn) - error", () => {
          {
            const val = ja("fn() end");

            _chai.assert.throws(() => {
              {
                val.eq(_core.dogma.nop());
              }
            });
          }
        });
        (0, _ethron.test)("eq(nil) - error", () => {
          {
            const val = ja("null");

            _chai.assert.throws(() => {
              {
                val.eq(null);
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("ne()", () => {
      {
        (0, _ethron.test)("ne(text)", params => {
          /* istanbul ignore next */
          _core.dogma.paramExpected("params", params, null);

          {
            const val = ja("bonjour");

            _chai.assert.strictEqual((0, _core.bind)(val, params.meth)("ciao"), val);
          }
        }).forEach({
          ["sub"]: "ne()",
          ["meth"]: "ne"
        }, {
          ["sub"]: "neq()",
          ["meth"]: "neq"
        });
        (0, _ethron.test)("ne(text) - error", () => {
          {
            const val = ja("bonjour");

            _chai.assert.throws(() => {
              {
                val.ne("bonjour");
              }
            });
          }
        });
        (0, _ethron.test)("ne(bool)", () => {
          {
            const val = ja("true");

            _chai.assert.strictEqual(val.ne(true), val);
          }
        });
        (0, _ethron.test)("ne(num)", () => {
          {
            const val = ja("123");

            _chai.assert.strictEqual(val.ne(123), val);
          }
        });
        (0, _ethron.test)("ne(list)", () => {
          {
            const val = ja("[1, 2, 3]");

            _chai.assert.strictEqual(val.ne([1, 2, 3]), val);
          }
        });
        (0, _ethron.test)("ne(map)", () => {
          {
            const val = ja("{x = 1, y = 2}");

            _chai.assert.strictEqual(val.ne({
              ["x"]: 1,
              ["y"]: 2
            }), val);
          }
        });
        (0, _ethron.test)("ne(fn)", () => {
          {
            const val = ja("fn() end");

            _chai.assert.strictEqual(val.ne(_core.dogma.nop()), val);
          }
        });
        (0, _ethron.test)("ne(nil) - error", () => {
          {
            const val = ja("null");

            _chai.assert.strictEqual(val.ne(null), val);
          }
        });
      }
    });
    (0, _ethron.suite)("sameAs()", () => {
      {
        (0, _ethron.test)("sameAs(text)", () => {
          {
            const val = ja("au revoir");

            _chai.assert.strictEqual(val.sameAs("au revoir"), val);
          }
        });
        (0, _ethron.test)("sameAs(text) - error", () => {
          {
            const val = ja("au revoir");

            _chai.assert.throws(() => {
              {
                val.sameAs("ciao");
              }
            });
          }
        });
        (0, _ethron.test)("sameAs(bool) - error", () => {
          {
            const val = ja("true");

            _chai.assert.throws(() => {
              {
                val.sameAs(true);
              }
            });
          }
        });
        (0, _ethron.test)("sameAs(num) - error", () => {
          {
            const val = ja("123");

            _chai.assert.throws(() => {
              {
                val.sameAs(123);
              }
            });
          }
        });
        (0, _ethron.test)("sameAs(list) - error", () => {
          {
            const val = ja("[1, 2, 3]");

            _chai.assert.throws(() => {
              {
                val.sameAs([1, 2, 3]);
              }
            });
          }
        });
        (0, _ethron.test)("sameAs(map) - error", () => {
          {
            const val = ja("{x = 1, y = 2}");

            _chai.assert.throws(() => {
              {
                val.sameAs({
                  ["x"]: 1,
                  ["y"]: 2
                });
              }
            });
          }
        });
        (0, _ethron.test)("sameAs(fn) - error", () => {
          {
            const val = ja("null");

            _chai.assert.throws(() => {
              {
                val.sameAs(null);
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("notSameAs()", () => {
      {
        (0, _ethron.test)("notSameAs(text)", () => {
          {
            const val = ja("ciao");

            _chai.assert.strictEqual(val.notSameAs("bonjour"), val);
          }
        });
        (0, _ethron.test)("notSameAs(text) - error", () => {
          {
            const val = ja("ciao");

            _chai.assert.throws(() => {
              {
                val.notSameAs("ciao");
              }
            });
          }
        });
        (0, _ethron.test)("notSameAs(bool)", () => {
          {
            const val = ja("true");

            _chai.assert.strictEqual(val.notSameAs(true), val);
          }
        });
        (0, _ethron.test)("notSameAs(num)", () => {
          {
            const val = ja("123");

            _chai.assert.strictEqual(val.notSameAs(123), val);
          }
        });
        (0, _ethron.test)("notSameAs(list)", () => {
          {
            const val = ja("[1, 2, 3]");

            _chai.assert.strictEqual(val.notSameAs([1, 2, 3]), val);
          }
        });
        (0, _ethron.test)("notSameAs(map)", () => {
          {
            const val = ja("{x = 1, y = 2}");

            _chai.assert.strictEqual(val.notSameAs({
              ["x"]: 1,
              ["y"]: 2
            }), val);
          }
        });
        (0, _ethron.test)("notSameAs(fn)", () => {
          {
            const val = ja("null");

            _chai.assert.strictEqual(val.notSameAs(null), val);
          }
        });
      }
    });
    (0, _ethron.suite)("gt()", () => {
      {
        (0, _ethron.test)("gt(text)", () => {
          {
            const val = ja("b");

            _chai.assert.strictEqual(val.gt("a"), val);
          }
        });
        (0, _ethron.test)("gt(text) - error when a < b", () => {
          {
            const val = ja("a");

            _chai.assert.throws(() => {
              {
                val.gt("b");
              }
            });
          }
        });
        (0, _ethron.test)("gt(text) - error when a == b", () => {
          {
            const val = ja("b");

            _chai.assert.throws(() => {
              {
                val.gt("b");
              }
            });
          }
        });
        (0, _ethron.test)("gt(bool) - error", () => {
          {
            const val = ja("true");

            _chai.assert.throws(() => {
              {
                val.gt(true);
              }
            });
          }
        });
        (0, _ethron.test)("gt(num) - error", () => {
          {
            const val = ja("1234");

            _chai.assert.throws(() => {
              {
                val.gt(0);
              }
            });
          }
        });
        (0, _ethron.test)("gt(list) - error", () => {
          {
            const val = ja("[1, 2, 3]");

            _chai.assert.throws(() => {
              {
                val.gt([1]);
              }
            });
          }
        });
        (0, _ethron.test)("gt(map) - error", () => {
          {
            const val = ja("{x = 1, y = 2}");

            _chai.assert.throws(() => {
              {
                val.gt({
                  ["x"]: 0
                });
              }
            });
          }
        });
        (0, _ethron.test)("gt(fn) - error", () => {
          {
            const val = ja("fn() end");

            _chai.assert.throws(() => {
              {
                val.gt(_core.dogma.nop());
              }
            });
          }
        });
        (0, _ethron.test)("gt(nil) - error", () => {
          {
            const val = ja("null");

            _chai.assert.throws(() => {
              {
                val.gt(null);
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("ge()", () => {
      {
        (0, _ethron.test)("ge(text) - when greater than", () => {
          {
            const val = ja("b");

            _chai.assert.strictEqual(val.ge("a"), val);
          }
        });
        (0, _ethron.test)("ge(text) - when equal to", () => {
          {
            const val = ja("b");

            _chai.assert.strictEqual(val.ge("b"), val);
          }
        });
        (0, _ethron.test)("ge(text) - error when a < b", () => {
          {
            const val = ja("a");

            _chai.assert.throws(() => {
              {
                val.ge("b");
              }
            });
          }
        });
        (0, _ethron.test)("ge(bool) - error", () => {
          {
            const val = ja("true");

            _chai.assert.throws(() => {
              {
                val.ge(true);
              }
            });
          }
        });
        (0, _ethron.test)("ge(num) - error", () => {
          {
            const val = ja("1234");

            _chai.assert.throws(() => {
              {
                val.ge(0);
              }
            });
          }
        });
        (0, _ethron.test)("ge(list) - error", () => {
          {
            const val = ja("[1, 2, 3]");

            _chai.assert.throws(() => {
              {
                val.ge([1]);
              }
            });
          }
        });
        (0, _ethron.test)("ge(map) - error", () => {
          {
            const val = ja("{x = 1, y = 2}");

            _chai.assert.throws(() => {
              {
                val.ge({
                  ["x"]: 0
                });
              }
            });
          }
        });
        (0, _ethron.test)("ge(fn) - error", () => {
          {
            const val = ja("fn() end");

            _chai.assert.throws(() => {
              {
                val.ge(_core.dogma.nop());
              }
            });
          }
        });
        (0, _ethron.test)("ge(nil) - error", () => {
          {
            const val = ja("null");

            _chai.assert.throws(() => {
              {
                val.ge(null);
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("lt()", () => {
      {
        (0, _ethron.test)("lt(text)", () => {
          {
            const val = ja("b");

            _chai.assert.strictEqual(val.lt("c"), val);
          }
        });
        (0, _ethron.test)("lt(text) - error when greater than", () => {
          {
            const val = ja("b");

            _chai.assert.throws(() => {
              {
                val.lt("a");
              }
            });
          }
        });
        (0, _ethron.test)("ge(text) - error when equal to", () => {
          {
            const val = ja("b");

            _chai.assert.throws(() => {
              {
                val.lt("b");
              }
            });
          }
        });
        (0, _ethron.test)("lt(bool) - error", () => {
          {
            const val = ja("true");

            _chai.assert.throws(() => {
              {
                val.lt(true);
              }
            });
          }
        });
        (0, _ethron.test)("lt(num) - error", () => {
          {
            const val = ja("1234");

            _chai.assert.throws(() => {
              {
                val.lt(4321);
              }
            });
          }
        });
        (0, _ethron.test)("lt(list) - error", () => {
          {
            const val = ja("[1, 2, 3]");

            _chai.assert.throws(() => {
              {
                val.lt([1, 2, 3, 4]);
              }
            });
          }
        });
        (0, _ethron.test)("lt(map) - error", () => {
          {
            const val = ja("{x = 1, y = 2}");

            _chai.assert.throws(() => {
              {
                val.lt({
                  ["x"]: 0,
                  ["y"]: 1,
                  ["z"]: 2
                });
              }
            });
          }
        });
        (0, _ethron.test)("lt(fn) - error", () => {
          {
            const val = ja("fn() end");

            _chai.assert.throws(() => {
              {
                val.lt(_core.dogma.nop());
              }
            });
          }
        });
        (0, _ethron.test)("lt(nil) - error", () => {
          {
            const val = ja("null");

            _chai.assert.throws(() => {
              {
                val.lt(null);
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("le()", () => {
      {
        (0, _ethron.test)("le(text) - when less than", () => {
          {
            const val = ja("b");

            _chai.assert.strictEqual(val.le("c"), val);
          }
        });
        (0, _ethron.test)("le(text) - when equal to", () => {
          {
            const val = ja("b");

            _chai.assert.strictEqual(val.le("b"), val);
          }
        });
        (0, _ethron.test)("le(text) - error when greater than", () => {
          {
            const val = ja("b");

            _chai.assert.throws(() => {
              {
                val.le("a");
              }
            });
          }
        });
        (0, _ethron.test)("le(bool) - error", () => {
          {
            const val = ja("true");

            _chai.assert.throws(() => {
              {
                val.le(true);
              }
            });
          }
        });
        (0, _ethron.test)("le(num) - error", () => {
          {
            const val = ja("1234");

            _chai.assert.throws(() => {
              {
                val.le(0);
              }
            });
          }
        });
        (0, _ethron.test)("le(list) - error", () => {
          {
            const val = ja("[1, 2, 3]");

            _chai.assert.throws(() => {
              {
                val.le([1]);
              }
            });
          }
        });
        (0, _ethron.test)("le(map) - error", () => {
          {
            const val = ja("{x = 1, y = 2}");

            _chai.assert.throws(() => {
              {
                val.le({
                  ["x"]: 0
                });
              }
            });
          }
        });
        (0, _ethron.test)("le(fn) - error", () => {
          {
            const val = ja("fn() end");

            _chai.assert.throws(() => {
              {
                val.le(_core.dogma.nop());
              }
            });
          }
        });
        (0, _ethron.test)("le(nil) - error", () => {
          {
            const val = ja("null");

            _chai.assert.throws(() => {
              {
                val.le(null);
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("between", () => {
      {
        (0, _ethron.test)("between(text, text) - middle", () => {
          {
            const val = ja("b");

            _chai.assert.strictEqual(val.between("a", "c"), val);
          }
        });
        (0, _ethron.test)("between(text, text) - equal to start", () => {
          {
            const val = ja("a");

            _chai.assert.strictEqual(val.between("a", "c"), val);
          }
        });
        (0, _ethron.test)("between(text, text) - equal to end", () => {
          {
            const val = ja("c");

            _chai.assert.strictEqual(val.between("a", "c"), val);
          }
        });
        (0, _ethron.test)("between(text, text) - error", () => {
          {
            const val = ja("d");

            _chai.assert.throws(() => {
              {
                val.between("a", "c");
              }
            });
          }
        });
        (0, _ethron.test)("between(bool, bool) - error", () => {
          {
            const val = ja("g");

            _chai.assert.throws(() => {
              {
                val.between(false, true);
              }
            });
          }
        });
        (0, _ethron.test)("between(num, num) - error", () => {
          {
            const val = ja("2");

            _chai.assert.throws(() => {
              {
                val.between(1, 3);
              }
            });
          }
        });
        (0, _ethron.test)("between(list, list) - error", () => {
          {
            const val = ja("[2]");

            _chai.assert.throws(() => {
              {
                val.between([], [1, 2, 3]);
              }
            });
          }
        });
        (0, _ethron.test)("between(map, map) - error", () => {
          {
            const val = ja("{x = 1}");

            _chai.assert.throws(() => {
              {
                val.between({}, {
                  ["x"]: 1,
                  ["y"]: 2
                });
              }
            });
          }
        });
        (0, _ethron.test)("between(nil, nil) - error", () => {
          {
            const val = ja("null");

            _chai.assert.throws(() => {
              {
                val.between(null, null);
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("notBetween", () => {
      {
        (0, _ethron.test)("notBetween(text, text) - error when middle", () => {
          {
            const val = ja("b");

            _chai.assert.throws(() => {
              {
                val.notBetween("a", "c");
              }
            });
          }
        });
        (0, _ethron.test)("notBetween(text, text) - error when equal to start", () => {
          {
            const val = ja("a");

            _chai.assert.throws(() => {
              {
                val.notBetween("a", "c");
              }
            });
          }
        });
        (0, _ethron.test)("notBetween(text, text) - error when equal to end", () => {
          {
            const val = ja("c");

            _chai.assert.throws(() => {
              {
                val.notBetween("a", "c");
              }
            });
          }
        });
        (0, _ethron.test)("notBetween(text, text)", () => {
          {
            const val = ja("d");

            _chai.assert.strictEqual(val.notBetween("a", "c"), val);
          }
        });
        (0, _ethron.test)("notBetween(bool, bool)", () => {
          {
            const val = ja("g");

            _chai.assert.strictEqual(val.notBetween(false, true), val);
          }
        });
        (0, _ethron.test)("notBetween(num, num)", () => {
          {
            const val = ja("2");

            _chai.assert.strictEqual(val.notBetween(1, 3), val);
          }
        });
        (0, _ethron.test)("notBetween(list, list)", () => {
          {
            const val = ja("[2]");

            _chai.assert.strictEqual(val.notBetween([], [1, 2, 3]), val);
          }
        });
        (0, _ethron.test)("notBetween(map, map)", () => {
          {
            const val = ja("{x = 1}");

            _chai.assert.strictEqual(val.notBetween({}, {
              ["x"]: 1,
              ["y"]: 2
            }), val);
          }
        });
        (0, _ethron.test)("notBetween(nil, nil) - error", () => {
          {
            const val = ja("null");

            _chai.assert.strictEqual(val.notBetween(null, null), val);
          }
        });
      }
    });
    (0, _ethron.suite)("includes()", () => {
      {
        (0, _ethron.test)("includes(text)", () => {
          {
            const val = ja("belle journée");

            _chai.assert.strictEqual(val.includes("lle"), val);
          }
        });
        (0, _ethron.test)("inludes(text) - error", () => {
          {
            const val = ja("belle journée");

            _chai.assert.throws(() => {
              {
                val.includes("nop");
              }
            });
          }
        });
        (0, _ethron.test)("includes([]) - error", () => {
          {
            const val = ja("belle journée");

            _chai.assert.throws(() => {
              {
                val.includes([]);
              }
            });
          }
        });
        (0, _ethron.test)("includes(text[])", () => {
          {
            const val = ja("belle journée");

            _chai.assert.strictEqual(val.includes(["elle", "our"]), val);
          }
        });
        (0, _ethron.test)("includes(text[]) - error when none", () => {
          {
            const val = ja("belle journée");

            _chai.assert.throws(() => {
              {
                val.includes(["none", "nothing"]);
              }
            });
          }
        });
        (0, _ethron.test)("includes(text[]) - error when something isn't included", () => {
          {
            const val = ja("belle journée");

            _chai.assert.throws(() => {
              {
                val.includes(["elle", "ella"]);
              }
            });
          }
        });
        (0, _ethron.test)("includes(bool)", () => {
          {
            const val = ja("truefalse");

            _chai.assert.strictEqual(val.includes(true), val);
          }
        });
        (0, _ethron.test)("includes(num)", () => {
          {
            const val = ja("123456");

            _chai.assert.strictEqual(val.includes(2), val);
          }
        });
        (0, _ethron.test)("includes(list)", () => {
          {
            const val = ja("[[1, 2, 3], [4, 5, 6]]");

            _chai.assert.strictEqual(val.includes([1, 2, 3]), val);
          }
        });
      }
    });
    (0, _ethron.suite)("isEmpty()", () => {
      {
        (0, _ethron.test)("isEmpty()", () => {
          {
            const val = ja("");

            _chai.assert.strictEqual(val.isEmpty(), val);
          }
        });
        (0, _ethron.test)("isEmpty() - error", () => {
          {
            const val = ja("hi");

            _chai.assert.throws(() => {
              {
                val.isEmpty();
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("isNotEmpty()", () => {
      {
        (0, _ethron.test)("isNotEmpty()", () => {
          {
            const val = ja("hi");

            _chai.assert.strictEqual(val.isNotEmpty(), val);
          }
        });
        (0, _ethron.test)("isNotEmpty() - error", () => {
          {
            const val = ja("");

            _chai.assert.throws(() => {
              {
                val.isNotEmpty();
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("len()", () => {
      {
        (0, _ethron.test)("len(num)", () => {
          {
            const val = ja("hi!");

            _chai.assert.strictEqual(val.len(3), val);
          }
        });
        (0, _ethron.test)("len(num) - error", () => {
          {
            const val = ja("hi!");

            _chai.assert.throws(() => {
              {
                val.len(4);
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("notLen()", () => {
      {
        (0, _ethron.test)("notLen(num)", () => {
          {
            const val = ja("hi!");

            _chai.assert.strictEqual(val.notLen(2), val);
          }
        });
        (0, _ethron.test)("notLen(num) - error", () => {
          {
            const val = ja("hi!");

            _chai.assert.throws(() => {
              {
                val.notLen(3);
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("startsWith()", () => {
      {
        (0, _ethron.test)("ok", () => {
          {
            const val = ja("supertramp");

            _chai.assert.strictEqual(val.startsWith("super"), val);
          }
        });
        (0, _ethron.test)("error", () => {
          {
            const val = ja("supertramp");

            _chai.assert.throws(() => {
              {
                val.startsWith("tramp");
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("notStartsWith()", () => {
      {
        (0, _ethron.test)("ok", () => {
          {
            const val = ja("supertramp");

            _chai.assert.strictEqual(val.notStartsWith("tramp"), val);
          }
        });
        (0, _ethron.test)("error", () => {
          {
            const val = ja("supertramp");

            _chai.assert.throws(() => {
              {
                val.notStartsWith("super");
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("endsWith()", () => {
      {
        (0, _ethron.test)("ok", () => {
          {
            const val = ja("supertramp");

            _chai.assert.strictEqual(val.endsWith("tramp"), val);
          }
        });
        (0, _ethron.test)("error", () => {
          {
            const val = ja("supertramp");

            _chai.assert.throws(() => {
              {
                val.endsWith("super");
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("notEndsWith()", () => {
      {
        (0, _ethron.test)("ok", () => {
          {
            const val = ja("supertramp");

            _chai.assert.strictEqual(val.notEndsWith("super"), val);
          }
        });
        (0, _ethron.test)("error", () => {
          {
            const val = ja("supertramp");

            _chai.assert.throws(() => {
              {
                val.notEndsWith("tramp");
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("like(pat)", () => {
      {
        (0, _ethron.test)("like(pat)", () => {
          {
            const val = ja("the national");

            _chai.assert.strictEqual(val.like("nat"), val);
          }
        });
        (0, _ethron.test)("like(pat) - error", () => {
          {
            const val = ja("the national");

            _chai.assert.throws(() => {
              {
                val.like("nit");
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("notLike(pat)", () => {
      {
        (0, _ethron.test)("notLike(pat)", () => {
          {
            const val = ja("the national");

            _chai.assert.strictEqual(val.notLike("nit"), val);
          }
        });
        (0, _ethron.test)("notLike(pat) - error", () => {
          {
            const val = ja("the national");

            _chai.assert.throws(() => {
              {
                val.notLike("nat");
              }
            });
          }
        });
      }
    });
  }
});