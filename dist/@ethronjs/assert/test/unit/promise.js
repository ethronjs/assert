"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

var _chai = require("chai");

const ja = _core.dogma.use(require("../../../../@ethronjs/assert"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    (0, _ethron.test)("isPromise()", () => {
      {
        const val = ja(_core.promise.resolve());

        _chai.assert.strictEqual(val.isPromise(), val);
      }
    });
    (0, _ethron.test)("isPromise() - error", () => {
      {
        const val = ja("hi!");

        _chai.assert.throws(() => {
          {
            val.isPromise();
          }
        });
      }
    });
    (0, _ethron.test)("isNotPromise() - error", () => {
      {
        const val = ja(_core.promise.resolve());

        _chai.assert.throws(() => {
          {
            val.isNotPromise();
          }
        });
      }
    });
    (0, _ethron.test)("isNotPromise()", () => {
      {
        const val = ja("hi!");

        _chai.assert.strictEqual(val.isNotPromise(), val);
      }
    });
  }
});