"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

var _chai = require("chai");

const ja = _core.dogma.use(require("../../../../@ethronjs/assert"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    (0, _ethron.test)("isString()", () => {
      {
        return {
          ["val"]: ja("text")
        };
      }
    }).assert(params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, _core.map);

      let {
        val
      } = params;
      {
        _chai.assert.strictEqual(val.isString(), val);
      }
    });
    (0, _ethron.test)("isNotString()", () => {
      {
        return {
          ["val"]: ja(true)
        };
      }
    }).assert(params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, _core.map);

      let {
        val
      } = params;
      {
        _chai.assert.strictEqual(val.isNotString(), val);
      }
    });
    (0, _ethron.test)("isNull()", () => {
      {
        return {
          ["val"]: ja(null)
        };
      }
    }).assert(params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, _core.map);

      let {
        val
      } = params;
      {
        _chai.assert.strictEqual(val.isNull(), val);
      }
    });
    (0, _ethron.test)("isNotNull()", () => {
      {
        return {
          ["val"]: ja("text")
        };
      }
    }).assert(params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, _core.map);

      let {
        val
      } = params;
      {
        _chai.assert.strictEqual(val.isNotNull(), val);
      }
    });
    (0, _ethron.test)("isArray()", () => {
      {
        return {
          ["val"]: ja([1, 2, 3])
        };
      }
    }).assert(params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, _core.map);

      let {
        val
      } = params;
      {
        _chai.assert.strictEqual(val.isArray(), val);
      }
    });
    (0, _ethron.test)("isNotArray()", () => {
      {
        return {
          ["val"]: ja({})
        };
      }
    }).assert(params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, _core.map);

      let {
        val
      } = params;
      {
        _chai.assert.strictEqual(val.isNotArray(), val);
      }
    });
    (0, _ethron.test)("isObject()", () => {
      {
        return {
          ["val"]: ja({})
        };
      }
    }).assert(params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, _core.map);

      let {
        val
      } = params;
      {
        _chai.assert.strictEqual(val.isObject(), val);
      }
    });
    (0, _ethron.test)("isNotObject()", () => {
      {
        return {
          ["val"]: ja("text")
        };
      }
    }).assert(params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, _core.map);

      let {
        val
      } = params;
      {
        _chai.assert.strictEqual(val.isNotObject(), val);
      }
    });
    (0, _ethron.test)("doesNotInclude()", () => {
      {
        return {
          ["val"]: ja([1, 2, 3])
        };
      }
    }).assert(params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, _core.map);

      let {
        val
      } = params;
      {
        _chai.assert.strictEqual(val.doesNotInclude(5), val);
      }
    });
    (0, _ethron.test)("doesNotStartWith()", () => {
      {
        return {
          ["val"]: ja("supertramp")
        };
      }
    }).assert(params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, _core.map);

      let {
        val
      } = params;
      {
        _chai.assert.strictEqual(val.doesNotStartWith("tramp"), val);
      }
    });
    (0, _ethron.test)("doesNotEndWith()", () => {
      {
        return {
          ["val"]: ja("supertramp")
        };
      }
    }).assert(params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, _core.map);

      let {
        val
      } = params;
      {
        _chai.assert.strictEqual(val.doesNotEndWith("super"), val);
      }
    });
  }
});