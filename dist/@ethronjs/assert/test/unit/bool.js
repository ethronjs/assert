"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

var _chai = require("chai");

const ja = _core.dogma.use(require("../../../../@ethronjs/assert"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    (0, _ethron.test)("isBool()", params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, null);

      {
        const val = ja(true);

        _chai.assert.strictEqual((0, _core.bind)(val, params.meth)(), val);
      }
    }).forEach({
      ["sub"]: "isBool()",
      ["meth"]: "isBool"
    }, {
      ["sub"]: "isBoolean",
      ["meth"]: "isBoolean"
    });
    (0, _ethron.test)("isNotBool() - error", params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, null);

      {
        const val = ja(true);

        _chai.assert.throws(() => {
          {
            (0, _core.bind)(val, params.meth)();
          }
        });
      }
    }).forEach({
      ["sub"]: "isNotBool()",
      ["meth"]: "isNotBool"
    }, {
      ["sub"]: "isNotBoolean()",
      ["meth"]: "isNotBoolean"
    });
    (0, _ethron.test)("isText() - error", () => {
      {
        const val = ja(true);

        _chai.assert.throws(() => {
          {
            val.isText();
          }
        });
      }
    });
    (0, _ethron.test)("isNotText()", () => {
      {
        const val = ja(true);

        _chai.assert.strictEqual(val.isNotText(), val);
      }
    });
    (0, _ethron.test)("isNum() - error", () => {
      {
        const val = ja(true);

        _chai.assert.throws(() => {
          {
            val.isNum();
          }
        });
      }
    });
    (0, _ethron.test)("isNotNum()", () => {
      {
        const val = ja(true);

        _chai.assert.strictEqual(val.isNotNum(), val);
      }
    });
    (0, _ethron.test)("isList() - error", () => {
      {
        const val = ja(true);

        _chai.assert.throws(() => {
          {
            val.isList();
          }
        });
      }
    });
    (0, _ethron.test)("isNotList()", () => {
      {
        const val = ja(true);

        _chai.assert.strictEqual(val.isNotList(), val);
      }
    });
    (0, _ethron.test)("isSet() - error", () => {
      {
        const val = ja(true);

        _chai.assert.throws(() => {
          {
            val.isSet();
          }
        });
      }
    });
    (0, _ethron.test)("isNotSet()", () => {
      {
        const val = ja(true);

        _chai.assert.strictEqual(val.isNotSet(), val);
      }
    });
    (0, _ethron.test)("isMap() - error", () => {
      {
        const val = ja(true);

        _chai.assert.throws(() => {
          {
            val.isMap();
          }
        });
      }
    });
    (0, _ethron.test)("isNotMap()", () => {
      {
        const val = ja(true);

        _chai.assert.strictEqual(val.isNotMap(), val);
      }
    });
    (0, _ethron.test)("isFn() - error", () => {
      {
        const val = ja(true);

        _chai.assert.throws(() => {
          {
            val.isFn();
          }
        });
      }
    });
    (0, _ethron.test)("isNotFn()", () => {
      {
        const val = ja(true);

        _chai.assert.strictEqual(val.isNotFn(), val);
      }
    });
    (0, _ethron.test)("isCallable() - error", () => {
      {
        const val = ja(true);

        _chai.assert.throws(() => {
          {
            val.isCallable();
          }
        });
      }
    });
    (0, _ethron.test)("isNotCallable()", () => {
      {
        const val = ja(true);

        _chai.assert.strictEqual(val.isNotCallable(), val);
      }
    });
    (0, _ethron.suite)("eq()", () => {
      {
        (0, _ethron.test)("eq() - true equal to true", () => {
          {
            const val = ja(true);

            _chai.assert.strictEqual(val.eq(true), val);
          }
        });
        (0, _ethron.test)("eq() - error when true not equal to false", () => {
          {
            const val = ja(true);

            _chai.assert.throws(() => {
              {
                val.eq(false);
              }
            });
          }
        });
        (0, _ethron.test)("eq() - error when text", () => {
          {
            const val = ja(true);

            _chai.assert.throws(() => {
              {
                val.eq("true");
              }
            });
          }
        });
        (0, _ethron.test)("eq() - error when num", () => {
          {
            const val = ja(false);

            _chai.assert.throws(() => {
              {
                val.eq([]);
              }
            });
          }
        });
        (0, _ethron.test)("eq() - error when map", () => {
          {
            const val = ja(false);

            _chai.assert.throws(() => {
              {
                val.eq({});
              }
            });
          }
        });
        (0, _ethron.test)("eq() - error when fn", () => {
          {
            const val = ja(false);

            _chai.assert.throws(() => {
              {
                val.eq(_core.dogma.nop());
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("ne()", () => {
      {
        (0, _ethron.test)("ne() - true not equal to false", () => {
          {
            const val = ja(true);

            _chai.assert.strictEqual(val.ne(false), val);
          }
        });
        (0, _ethron.test)("ne() - error when true not equal to true", () => {
          {
            const val = ja(true);

            _chai.assert.throws(() => {
              {
                val.ne(true);
              }
            });
          }
        });
        (0, _ethron.test)("ne() - when text", () => {
          {
            const val = ja(true);

            _chai.assert.strictEqual(val.ne("true"), val);
          }
        });
        (0, _ethron.test)("ne() - when num", () => {
          {
            const val = ja(false);

            _chai.assert.strictEqual(val.ne([]), val);
          }
        });
        (0, _ethron.test)("ne() - when map", () => {
          {
            const val = ja(false);

            _chai.assert.strictEqual(val.ne({}), val);
          }
        });
        (0, _ethron.test)("ne() - when fn", () => {
          {
            const val = ja(false);

            _chai.assert.strictEqual(val.ne(_core.dogma.nop()), val);
          }
        });
      }
    });
    (0, _ethron.suite)("sameAs()", () => {
      {
        const val = ja(true);
        (0, _ethron.test)("sameAs()", () => {
          {
            _chai.assert.strictEqual(val.sameAs(true), val);
          }
        });
        (0, _ethron.test)("sameAs() - error with bool", () => {
          {
            _chai.assert.throws(() => {
              {
                val.sameAs(false);
              }
            });
          }
        });
        (0, _ethron.test)("sameAs() - error with text", () => {
          {
            _chai.assert.throws(() => {
              {
                val.sameAs("true");
              }
            });
          }
        });
        (0, _ethron.test)("sameAs() - error with num", () => {
          {
            _chai.assert.throws(() => {
              {
                val.sameAs(1);
              }
            });
          }
        });
        (0, _ethron.test)("sameAs() - error with list", () => {
          {
            _chai.assert.throws(() => {
              {
                val.sameAs([1, 2, 3]);
              }
            });
          }
        });
        (0, _ethron.test)("sameAs() - error with map", () => {
          {
            _chai.assert.throws(() => {
              {
                val.sameAs({
                  ["x"]: 1
                });
              }
            });
          }
        });
        (0, _ethron.test)("sameAs() - error with fn", () => {
          {
            _chai.assert.throws(() => {
              {
                val.sameAs(_core.dogma.nop());
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("notSameAs()", () => {
      {
        const val = ja(true);
        (0, _ethron.test)("notSameAs()", () => {
          {
            _chai.assert.strictEqual(val.notSameAs(false), val);
          }
        });
        (0, _ethron.test)("notSameAs() - error with bool", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notSameAs(true);
              }
            });
          }
        });
        (0, _ethron.test)("notSameAs() - with text", () => {
          {
            _chai.assert.strictEqual(val.notSameAs("true"), val);
          }
        });
        (0, _ethron.test)("notSameAs() - with num", () => {
          {
            _chai.assert.strictEqual(val.notSameAs(1), val);
          }
        });
        (0, _ethron.test)("notSameAs() - with list", () => {
          {
            _chai.assert.strictEqual(val.notSameAs([1, 2, 3]), val);
          }
        });
        (0, _ethron.test)("notSameAs() - with map", () => {
          {
            _chai.assert.strictEqual(val.notSameAs({
              ["x"]: 1
            }), val);
          }
        });
        (0, _ethron.test)("notSameAs() - with fn", () => {
          {
            _chai.assert.strictEqual(val.notSameAs(_core.dogma.nop()), val);
          }
        });
      }
    });
    (0, _ethron.suite)("lt()", () => {
      {
        const val = ja(true);
        (0, _ethron.test)("lt(bool) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.lt(false);
              }
            });
          }
        });
        (0, _ethron.test)("lt(text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.lt("true");
              }
            });
          }
        });
        (0, _ethron.test)("lt(num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.lt(1);
              }
            });
          }
        });
        (0, _ethron.test)("lt(nil) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.lt(null);
              }
            });
          }
        });
        (0, _ethron.test)("lt(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.lt([]);
              }
            });
          }
        });
        (0, _ethron.test)("lt(map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.lt({});
              }
            });
          }
        });
        (0, _ethron.test)("lt(fn) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.lt(_core.dogma.nop());
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("le()", () => {
      {
        const val = ja(true);
        (0, _ethron.test)("le(bool)", () => {
          {
            _chai.assert.throws(() => {
              {
                val.le(false);
              }
            });
          }
        });
        (0, _ethron.test)("le(text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.le("true");
              }
            });
          }
        });
        (0, _ethron.test)("le(num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.le(1);
              }
            });
          }
        });
        (0, _ethron.test)("le(nil) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.le(null);
              }
            });
          }
        });
        (0, _ethron.test)("le(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.le([]);
              }
            });
          }
        });
        (0, _ethron.test)("le(map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.le({});
              }
            });
          }
        });
        (0, _ethron.test)("le(fn) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.le(_core.dogma.nop());
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("gt()", () => {
      {
        const val = ja(true);
        (0, _ethron.test)("gt(bool) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.gt(false);
              }
            });
          }
        });
        (0, _ethron.test)("gt(text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.gt("true");
              }
            });
          }
        });
        (0, _ethron.test)("gt(num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.gt(1);
              }
            });
          }
        });
        (0, _ethron.test)("gt(nil) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.gt(null);
              }
            });
          }
        });
        (0, _ethron.test)("gt(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.gt([]);
              }
            });
          }
        });
        (0, _ethron.test)("gt(map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.gt({});
              }
            });
          }
        });
        (0, _ethron.test)("gt(fn) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.gt(_core.dogma.nop());
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("ge()", () => {
      {
        const val = ja(true);
        (0, _ethron.test)("ge(bool) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.ge(false);
              }
            });
          }
        });
        (0, _ethron.test)("ge(text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.ge("true");
              }
            });
          }
        });
        (0, _ethron.test)("ge(num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.ge(1);
              }
            });
          }
        });
        (0, _ethron.test)("ge(nil) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.ge(null);
              }
            });
          }
        });
        (0, _ethron.test)("ge(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.ge([]);
              }
            });
          }
        });
        (0, _ethron.test)("ge(map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.ge({});
              }
            });
          }
        });
        (0, _ethron.test)("ge(fn) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.ge(_core.dogma.nop());
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("between()", () => {
      {
        const val = ja(true);
        (0, _ethron.test)("between(bool, bool) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.between(true, false);
              }
            });
          }
        });
        (0, _ethron.test)("between(text, text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.between("true", "false");
              }
            });
          }
        });
        (0, _ethron.test)("between(num, num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.between(0, 1);
              }
            });
          }
        });
        (0, _ethron.test)("between(list, list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.between([], [true, false]);
              }
            });
          }
        });
        (0, _ethron.test)("between(map, map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.between({}, {
                  ["x"]: 1,
                  ["y"]: 2
                });
              }
            });
          }
        });
        (0, _ethron.test)("between(nil, nil) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.between(null, null);
              }
            });
          }
        });
        (0, _ethron.test)("between(fn, fn) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.between(_core.dogma.nop(), _core.dogma.nop());
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("notBetween()", () => {
      {
        const val = ja(true);
        (0, _ethron.test)("notBetween(bool, bool)", () => {
          {
            _chai.assert.strictEqual(val.notBetween(true, false), val);
          }
        });
        (0, _ethron.test)("notBetween(text, text)", () => {
          {
            _chai.assert.strictEqual(val.notBetween("true", "false"), val);
          }
        });
        (0, _ethron.test)("notBetween(num, num)", () => {
          {
            _chai.assert.strictEqual(val.notBetween(0, 1), val);
          }
        });
        (0, _ethron.test)("notBetween(list, list)", () => {
          {
            _chai.assert.strictEqual(val.notBetween([], [true, false]), val);
          }
        });
        (0, _ethron.test)("notBetween(map, map)", () => {
          {
            _chai.assert.strictEqual(val.notBetween({}, {
              ["x"]: 1,
              ["y"]: 2
            }), val);
          }
        });
        (0, _ethron.test)("notBetween(nil, nil)", () => {
          {
            _chai.assert.strictEqual(val.notBetween(null, null), val);
          }
        });
        (0, _ethron.test)("notBetween(fn, fn)", () => {
          {
            _chai.assert.strictEqual(val.notBetween(_core.dogma.nop(), _core.dogma.nop()), val);
          }
        });
      }
    });
    (0, _ethron.suite)("includes()", () => {
      {
        const val = ja(true);
        (0, _ethron.test)("includes(bool) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes(true);
              }
            });
          }
        });
        (0, _ethron.test)("includes(text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes("true");
              }
            });
          }
        });
        (0, _ethron.test)("includes(num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes(123);
              }
            });
          }
        });
        (0, _ethron.test)("includes(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes([true]);
              }
            });
          }
        });
        (0, _ethron.test)("includes(map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes({
                  ["x"]: true
                });
              }
            });
          }
        });
        (0, _ethron.test)("includes(nil) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes(null);
              }
            });
          }
        });
        (0, _ethron.test)("includes(fn) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes(_core.dogma.nop());
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("notIncludes()", () => {
      {
        const val = ja(true);
        (0, _ethron.test)("notIncludes(bool)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes(true), val);
          }
        });
        (0, _ethron.test)("notIncludes(text)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes("true"), val);
          }
        });
        (0, _ethron.test)("notIncludes(num)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes(123), val);
          }
        });
        (0, _ethron.test)("notIncludes(list)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes([true]), val);
          }
        });
        (0, _ethron.test)("notIncludes(map)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes({
              ["x"]: true
            }), val);
          }
        });
        (0, _ethron.test)("notIncludes(nil)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes(null), val);
          }
        });
        (0, _ethron.test)("notIncludes(fn)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes(_core.dogma.nop()), val);
          }
        });
      }
    });
    (0, _ethron.suite)("has()", () => {
      {
        const val = ja(true);
        (0, _ethron.test)("has(mem) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.has("xyz");
              }
            });
          }
        });
        (0, _ethron.test)("has(mems) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.has(["abc", "xyz"]);
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("notHas()", () => {
      {
        const val = ja(true);
        (0, _ethron.test)("notHas(mem)", () => {
          {
            _chai.assert.strictEqual(val.notHas("xyz"), val);
          }
        });
        (0, _ethron.test)("notHas(mems)", () => {
          {
            _chai.assert.strictEqual(val.notHas(["abc", "xyz"]), val);
          }
        });
      }
    });
    (0, _ethron.test)("isEmpty() - error", () => {
      {
        const val = ja(true);

        _chai.assert.throws(() => {
          {
            val.isEmpty();
          }
        });
      }
    });
    (0, _ethron.test)("isNotEmpty() - error", () => {
      {
        const val = ja(true);

        _chai.assert.throws(() => {
          {
            val.isNotEmpty();
          }
        });
      }
    });
    (0, _ethron.test)("len() - error", () => {
      {
        const val = ja(true);

        _chai.assert.throws(() => {
          {
            val.len(0);
          }
        });
      }
    });
    (0, _ethron.test)("notLen() - error", () => {
      {
        const val = ja(true);

        _chai.assert.throws(() => {
          {
            val.notLen(0);
          }
        });
      }
    });
  }
});