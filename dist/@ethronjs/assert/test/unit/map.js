"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

var _chai = require("chai");

const ja = _core.dogma.use(require("../../../../@ethronjs/assert"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    (0, _ethron.test)("isMap()", () => {
      {
        const val = ja({});

        _chai.assert.strictEqual(val.isMap(), val);
      }
    });
    (0, _ethron.test)("isNotMap() - error", () => {
      {
        const val = ja({});

        _chai.assert.throws(() => {
          {
            val.isNotMap();
          }
        });
      }
    });
    (0, _ethron.suite)("has()", () => {
      {
        const val = ja({
          ["x"]: 1,
          ["y"]: 2,
          ["z"]: 3
        });
        (0, _ethron.test)("has(mem)", () => {
          {
            _chai.assert.strictEqual(val.has("x"), val);
          }
        });
        (0, _ethron.test)("has(mem) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.has("unknown");
              }
            });
          }
        });
        (0, _ethron.test)("has(mems:list)", () => {
          {
            _chai.assert.strictEqual(val.has(["x", "z"]), val);
          }
        });
        (0, _ethron.test)("has(mems:list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.has(["x", "a"]);
              }
            });
          }
        });
        (0, _ethron.test)("has(mems:map)", () => {
          {
            _chai.assert.strictEqual(val.has({
              ["x"]: 1,
              ["z"]: 3
            }), val);
          }
        });
        (0, _ethron.test)("has(mems:map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.has({
                  ["x"]: 1,
                  ["z"]: 2
                });
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("notHas()", () => {
      {
        const val = ja({
          ["x"]: 1,
          ["y"]: 2,
          ["z"]: 3
        });
        (0, _ethron.test)("doesNotHave(mem)", () => {
          {
            _chai.assert.strictEqual(val.doesNotHave("unknown"), val);
          }
        });
        (0, _ethron.test)("notHas(mem)", () => {
          {
            _chai.assert.strictEqual(val.notHas("unknown"), val);
          }
        });
        (0, _ethron.test)("notHas(mem) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notHas("x");
              }
            });
          }
        });
        (0, _ethron.test)("notHas(mems:list)", () => {
          {
            _chai.assert.strictEqual(val.notHas(["a", "b"]), val);
          }
        });
        (0, _ethron.test)("notHas(mems:list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notHas(["x", "a"]);
              }
            });
          }
        });
        (0, _ethron.test)("notHas(mems:map)", () => {
          {
            _chai.assert.strictEqual(val.notHas({
              ["x"]: 123,
              ["z"]: 321
            }), val);
          }
        });
        (0, _ethron.test)("notHas(mems:map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notHas({
                  ["x"]: 1,
                  ["z"]: 3
                });
              }
            });
          }
        });
      }
    });
  }
});