"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

var _chai = require("chai");

const ja = _core.dogma.use(require("../../../../@ethronjs/assert"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    const val = ja(null);
    (0, _ethron.test)("isNil()", () => {
      {
        _chai.assert.strictEqual(val.isNil(), val);
      }
    });
    (0, _ethron.test)("isNotNil()", () => {
      {
        _chai.assert.throws(() => {
          {
            val.isNotNil();
          }
        });
      }
    });
  }
});