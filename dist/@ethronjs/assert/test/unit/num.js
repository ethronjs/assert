"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

var _chai = require("chai");

const ja = _core.dogma.use(require("../../../../@ethronjs/assert"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    let val;
    (0, _ethron.init)(() => {
      {
        val = ja(123);
      }
    });
    (0, _ethron.test)("isNum() - error", params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, null);

      {
        _chai.assert.strictEqual((0, _core.bind)(val, params.meth)(), val);
      }
    }).forEach({
      ["sub"]: "isNum()",
      ["meth"]: "isNum"
    }, {
      ["sub"]: "isNumber()",
      ["meth"]: "isNumber"
    });
    (0, _ethron.test)("isNotNum()", params => {
      /* istanbul ignore next */
      _core.dogma.paramExpected("params", params, null);

      {
        _chai.assert.throws(() => {
          {
            (0, _core.bind)(val, params.meth)();
          }
        });
      }
    }).forEach({
      ["sub"]: "isNotNum()",
      ["meth"]: "isNotNum"
    }, {
      ["sub"]: "isNotNumber()",
      ["meth"]: "isNotNumber"
    });
  }
});