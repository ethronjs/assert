"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

var _chai = require("chai");

const ja = _core.dogma.use(require("../../../../@ethronjs/assert"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    (0, _ethron.suite)("isXxx()", () => {
      {
        const val = ja([1, 2, 3]);
        (0, _ethron.test)("isList()", () => {
          {
            _chai.assert.strictEqual(val.isList(), val);
          }
        });
        (0, _ethron.test)("isBool() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isBool();
              }
            });
          }
        });
        (0, _ethron.test)("isNum() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isNum();
              }
            });
          }
        });
        (0, _ethron.test)("isText() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isText();
              }
            });
          }
        });
        (0, _ethron.test)("isMap() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isMap();
              }
            });
          }
        });
        (0, _ethron.test)("isNil() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isNil();
              }
            });
          }
        });
        (0, _ethron.test)("isFn() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isFn();
              }
            });
          }
        });
        (0, _ethron.test)("isCallable() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isCallable();
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("isNotXxx()", () => {
      {
        const val = ja([1, 2, 3]);
        (0, _ethron.test)("isNotList() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isNotList();
              }
            });
          }
        });
        (0, _ethron.test)("isBool()", () => {
          {
            _chai.assert.strictEqual(val.isNotBool(), val);
          }
        });
        (0, _ethron.test)("isNum()", () => {
          {
            _chai.assert.strictEqual(val.isNotNum(), val);
          }
        });
        (0, _ethron.test)("isText()", () => {
          {
            _chai.assert.strictEqual(val.isNotText(), val);
          }
        });
        (0, _ethron.test)("isMap()", () => {
          {
            _chai.assert.strictEqual(val.isNotMap(), val);
          }
        });
        (0, _ethron.test)("isNil()", () => {
          {
            _chai.assert.strictEqual(val.isNotNil(), val);
          }
        });
        (0, _ethron.test)("isFn()", () => {
          {
            _chai.assert.strictEqual(val.isNotFn(), val);
          }
        });
        (0, _ethron.test)("isCallable()", () => {
          {
            _chai.assert.strictEqual(val.isNotCallable(), val);
          }
        });
      }
    });
    (0, _ethron.suite)("includes()", () => {
      {
        const val = ja([true, "ciao", 12, [1, 2, 3], {
          ["x"]: 1,
          ["y"]: 2,
          ["z"]: 3
        }]);
        (0, _ethron.test)("includes(bool)", () => {
          {
            _chai.assert.strictEqual(val.includes(true), val);
          }
        });
        (0, _ethron.test)("includes(bool) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes(false);
              }
            });
          }
        });
        (0, _ethron.test)("includes(num)", () => {
          {
            _chai.assert.strictEqual(val.includes(12), val);
          }
        });
        (0, _ethron.test)("includes(num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes(21);
              }
            });
          }
        });
        (0, _ethron.test)("includes(text)", () => {
          {
            _chai.assert.strictEqual(val.includes("ciao"), val);
          }
        });
        (0, _ethron.test)("includes(text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes("c");
              }
            });
          }
        });
        (0, _ethron.test)("includes(list)", () => {
          {
            _chai.assert.strictEqual(val.includes([[1, 2, 3]]), val);
          }
        });
        (0, _ethron.test)("includes(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes([[3, 2, 1]]);
              }
            });
          }
        });
        (0, _ethron.test)("includes(map)", () => {
          {
            _chai.assert.strictEqual(val.includes({
              ["x"]: 1,
              ["y"]: 2,
              ["z"]: 3
            }), val);
          }
        });
        (0, _ethron.test)("includes(map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes({
                  ["x"]: 1
                });
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("notIncludes()", () => {
      {
        const val = ja([true, "ciao", 12, [1, 2, 3], {
          ["x"]: 1,
          ["y"]: 2,
          ["z"]: 3
        }]);
        (0, _ethron.test)("notIncludes(bool)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes(false), val);
          }
        });
        (0, _ethron.test)("notIncludes(bool) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notIncludes(true);
              }
            });
          }
        });
        (0, _ethron.test)("notIncludes(num)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes(21), val);
          }
        });
        (0, _ethron.test)("notIncludes(num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notIncludes(12);
              }
            });
          }
        });
        (0, _ethron.test)("notIncludes(text)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes("oaic"), val);
          }
        });
        (0, _ethron.test)("notIncludes(text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notIncludes("ciao");
              }
            });
          }
        });
        (0, _ethron.test)("notIncludes(list)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes([[3, 2, 1]]), val);
          }
        });
        (0, _ethron.test)("notIncludes(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notIncludes([[1, 2, 3]]);
              }
            });
          }
        });
        (0, _ethron.test)("notIncludes(map)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes({
              ["x"]: 1
            }), val);
          }
        });
        (0, _ethron.test)("notIncludes(map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notIncludes({
                  ["x"]: 1,
                  ["y"]: 2,
                  ["z"]: 3
                });
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("isEmpty()", () => {
      {
        (0, _ethron.test)("isEmpty()", () => {
          {
            const val = ja([]);

            _chai.assert.strictEqual(val.isEmpty(), val);
          }
        });
        (0, _ethron.test)("isEmpty() - error", () => {
          {
            const val = ja([1, 2, 3]);

            _chai.assert.throws(() => {
              {
                val.isEmpty();
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("isNotEmpty()", () => {
      {
        (0, _ethron.test)("isNotEmpty()", () => {
          {
            const val = ja([1, 2, 3]);

            _chai.assert.strictEqual(val.isNotEmpty(), val);
          }
        });
        (0, _ethron.test)("isNotEmpty() - error", () => {
          {
            const val = ja([]);

            _chai.assert.throws(() => {
              {
                val.isNotEmpty();
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("len()", () => {
      {
        const val = ja([1, 2, 3]);
        (0, _ethron.test)("len(num)", () => {
          {
            _chai.assert.strictEqual(val.len(3), val);
          }
        });
        (0, _ethron.test)("len(list)", () => {
          {
            _chai.assert.strictEqual(val.len([1, 3, 5]), val);
          }
        });
        (0, _ethron.test)("len() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.len(2);
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("similarTo()", () => {
      {
        let val = ja([1, 2, 3]);
        (0, _ethron.test)("similarTo(list)", () => {
          {
            _chai.assert.strictEqual(val.similarTo([1, 3, 2]), val);
          }
        });
        (0, _ethron.test)("similarTo(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.similarTo([1, 3, 4]);
              }
            });
          }
        });
        (0, _ethron.test)("similarTo(list) - error by different len", () => {
          {
            _chai.assert.throws(() => {
              {
                val.similarTo([1, 3]);
              }
            });
          }
        });
        (0, _ethron.test)("[].similarTo(list)", () => {
          {
            val = ja([]);

            _chai.assert.strictEqual(val.similarTo([]), val);
          }
        });
      }
    });
    (0, _ethron.suite)("notSimilarTo()", () => {
      {
        const val = ja([1, 2, 3]);
        (0, _ethron.test)("notsimilarTo(list)", () => {
          {
            _chai.assert.strictEqual(val.notSimilarTo([1, 3, 5]), val);
          }
        });
        (0, _ethron.test)("notsimilar(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notSimilarTo([1, 3, 2]);
              }
            });
          }
        });
      }
    });
    (0, _ethron.suite)("forEach()", () => {
      {
        (0, _ethron.suite)("forEach(map)", () => {
          {
            const val = ja([{
              ["a"]: 1,
              ["b"]: 1
            }, {
              ["a"]: 1,
              ["b"]: 2
            }, {
              ["a"]: 1,
              ["c"]: 3
            }]);
            (0, _ethron.test)("ok", () => {
              {
                _chai.assert.strictEqual(val.forEach({
                  ["a"]: 1
                }), val);
              }
            });
            (0, _ethron.test)("error", () => {
              {
                _chai.assert.throws(() => {
                  {
                    val.forEach({
                      ["b"]: 1
                    });
                  }
                });
              }
            });
          }
        });
        (0, _ethron.suite)("forEach(func)", () => {
          {
            const val = ja([1, 2, 3, 4]);
            (0, _ethron.test)("ok", () => {
              {
                _chai.assert.strictEqual(val.forEach(i => {
                  /* istanbul ignore next */
                  _core.dogma.paramExpected("i", i, null);

                  {
                    _chai.assert.strictEqual(_core.dogma.is(i, _core.num), true);
                  }
                }), val);
              }
            });
            (0, _ethron.test)("error", () => {
              {
                _chai.assert.throws(() => {
                  {
                    val.forEach(i => {
                      /* istanbul ignore next */
                      _core.dogma.paramExpected("i", i, null);

                      {
                        _chai.assert.strictEqual(_core.dogma.is(i, _core.text), true);
                      }
                    });
                  }
                }, "expected false to equal true");
              }
            });
          }
        });
      }
    });
  }
});