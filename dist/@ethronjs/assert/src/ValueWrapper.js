"use strict";

var _core = require("@dogmalang/core");

const assert = _core.dogma.use(require("assert"));

const $ValueWrapper = class ValueWrapper {
  constructor(value, object = null) {
    Object.defineProperty(this, "value", {
      value: value,
      enum: true,
      writable: false
    });
    Object.defineProperty(this, "object", {
      value: object,
      enum: true,
      writable: false
    });
    {}
  }

};
const ValueWrapper = new Proxy($ValueWrapper, {
  apply(receiver, self, args) {
    return new $ValueWrapper(...args);
  }

});
module.exports = exports = ValueWrapper;
const Self = ValueWrapper;

Self.prototype.item = Self.prototype.mem = function (...args) {
  {
    const root = (0, _core.coalesce)(this.object, this.value);
    let item = root;

    for (const i of args) {
      item = _core.dogma.getItem(item, i);
    }

    return Self(item, root);
  }
};

Self.prototype.raises = function (err) {
  {
    {
      const [ok, res] = _core.dogma.peval(() => {
        return this.value();
      });

      if (ok) {
        _core.dogma.raise("function must raise error.");
      } else if (err && res != err) {
        _core.dogma.raise("function must raise '%s'. Raised: '%s'.", err, res);
      }
    }
  }
  return this;
};

Self.prototype.doesNotRaise = Self.prototype.notRaises = function (err) {
  {
    {
      const [ok, res] = _core.dogma.peval(() => {
        return this.value();
      });

      if (!ok) {
        if (err) {
          if (res == err) {
            _core.dogma.raise("function must not raise '%s'.", err);
          }
        } else {
          _core.dogma.raise("function must not raise error.");
        }
      }
    }
  }
  return this;
};

Self.prototype.is = Self.prototype.isInstanceOf = function (class_) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("class_", class_, null);

  {
    if (this.value != null) {
      if (_core.dogma.is(class_, _core.text)) {
        if (this.value.constructor.name != class_) {
          _core.dogma.raise("'%s' must be instance of '%s'.", this.value, class_);
        }
      } else {
        if (_core.dogma.isNot(this.value, class_)) {
          _core.dogma.raise("'%s' must be instance of '%s'.", this.value, class_);
        }
      }
    } else {
      _core.dogma.raise("'%s' must be instance of '%s'.", this.value === null ? "null" : "undefined", class_);
    }
  }
  return this;
};

Self.prototype.isNot = Self.prototype.isNotInstanceOf = function (class_) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("class_", class_, null);

  {
    if (this.value != null) {
      if (_core.dogma.is(class_, _core.text)) {
        if (this.value.constructor.name == class_) {
          _core.dogma.raise("'%s' must not be instance of '%s'.", this.value, class_);
        }
      } else {
        if (_core.dogma.is(this.value, class_)) {
          _core.dogma.raise("'%s' must not be instance of '%s'.", this.value, class_);
        }
      }
    }
  }
  return this;
};

Self.prototype.isNull = Self.prototype.isNil = function () {
  {
    if (this.value != null) {
      _core.dogma.raise("'%s' must be nil/null.", this.value);
    }
  }
  return this;
};

Self.prototype.isNotNull = Self.prototype.isNotNil = function () {
  {
    if (this.value == null) {
      _core.dogma.raise("'%s' must not be nil/null.", this.value);
    }
  }
  return this;
};

Self.prototype.isBoolean = Self.prototype.isBool = function () {
  {
    if (_core.dogma.isNot(this.value, _core.bool)) {
      _core.dogma.raise("'%s' must be boolean.", this.value);
    }
  }
  return this;
};

Self.prototype.isNotBoolean = Self.prototype.isNotBool = function () {
  {
    if (_core.dogma.is(this.value, _core.bool)) {
      _core.dogma.raise("'%s' must not be boolean.", this.value);
    }
  }
  return this;
};

Self.prototype.isString = Self.prototype.isText = function () {
  {
    if (_core.dogma.isNot(this.value, _core.text)) {
      _core.dogma.raise("'%s' must be text or string.", this.value);
    }
  }
  return this;
};

Self.prototype.isNotString = Self.prototype.isNotText = function () {
  {
    if (_core.dogma.is(this.value, _core.text)) {
      _core.dogma.raise("'%s' must not be text or string.", this.value);
    }
  }
  return this;
};

Self.prototype.isNumber = Self.prototype.isNum = function () {
  {
    if (_core.dogma.isNot(this.value, _core.num)) {
      _core.dogma.raise("'%s' must be number.", this.value);
    }
  }
  return this;
};

Self.prototype.isNotNumber = Self.prototype.isNotNum = function () {
  {
    if (_core.dogma.is(this.value, _core.num)) {
      _core.dogma.raise("'%s' must not be number.", this.value);
    }
  }
  return this;
};

Self.prototype.isArray = Self.prototype.isList = function () {
  {
    if (_core.dogma.isNot(this.value, _core.list)) {
      _core.dogma.raise("'%s' must be list or array.", this.value);
    }
  }
  return this;
};

Self.prototype.isNotArray = Self.prototype.isNotList = function () {
  {
    if (_core.dogma.is(this.value, _core.list)) {
      _core.dogma.raise("'%s' must not be list or array.", this.value);
    }
  }
  return this;
};

Self.prototype.isSet = function () {
  {
    if (_core.dogma.isNot(this.value, _core.set)) {
      _core.dogma.raise("'%s' must be set.", this.value);
    }
  }
  return this;
};

Self.prototype.isNotSet = function () {
  {
    if (_core.dogma.is(this.value, _core.set)) {
      _core.dogma.raise("'%s' must not be set.", this.value);
    }
  }
  return this;
};

Self.prototype.isObject = Self.prototype.isMap = function () {
  {
    if (_core.dogma.isNot(this.value, _core.map)) {
      _core.dogma.raise("'%s' must not map or object.", this.value);
    }
  }
  return this;
};

Self.prototype.isNotObject = Self.prototype.isNotMap = function () {
  {
    if (_core.dogma.is(this.value, _core.map)) {
      _core.dogma.raise("'%s' must not map or object.", this.value);
    }
  }
  return this;
};

Self.prototype.isFunction = Self.prototype.isFn = function () {
  {
    if (_core.dogma.isNot(this.value, _core.func)) {
      _core.dogma.raise("'%s' must be function.", this.value);
    }
  }
  return this;
};

Self.prototype.isNotFunction = Self.prototype.isNotFn = function () {
  {
    if (_core.dogma.is(this.value, _core.func)) {
      _core.dogma.raise("'%s' must not be function.", this.value);
    }
  }
  return this;
};

Self.prototype.isPromise = function () {
  {
    if (_core.dogma.isNot(this.value, _core.promise)) {
      _core.dogma.raise("'%s' must be promise.", this.value);
    }
  }
  return this;
};

Self.prototype.isNotPromise = function () {
  {
    if (_core.dogma.is(this.value, _core.promise)) {
      _core.dogma.raise("'%s' must not be promise.", this.value);
    }
  }
  return this;
};

Self.prototype.isCallable = function () {
  {
    if (_core.dogma.isNot(this.value, _core.func)) {
      _core.dogma.raise("'%s' must be callable.", this.value);
    }
  }
  return this;
};

Self.prototype.isNotCallable = function () {
  {
    if (_core.dogma.is(this.value, _core.func)) {
      _core.dogma.raise("'%s' must not be callable.", this.value);
    }
  }
  return this;
};

Self.prototype.eq = function (val) {
  {
    if ((0, _core.typename)(this.value) != (0, _core.typename)(val) || !_core.dogma.getItem(_core.dogma.peval(() => {
      return assert.deepEqual(this.value, val);
    }), 0)) {
      _core.dogma.raise("%s must be equal to %s.", (0, _core.fmt)(this.value), (0, _core.fmt)(val));
    }
  }
  return this;
};

Self.prototype.neq = Self.prototype.ne = function (val) {
  {
    if ((0, _core.typename)(this.value) == (0, _core.typename)(val) && _core.dogma.getItem(_core.dogma.peval(() => {
      return assert.deepEqual(this.value, val);
    }), 0)) {
      _core.dogma.raise("%s must not be equal to %s.", (0, _core.fmt)(this.value), (0, _core.fmt)(val));
    }
  }
  return this;
};

Self.prototype.sameAs = function (val) {
  {
    if (this.value !== val) {
      _core.dogma.raise("%s must be same as %s.", (0, _core.fmt)(this.value), (0, _core.fmt)(val));
    }
  }
  return this;
};

Self.prototype.notSameAs = function (val) {
  {
    if (this.value === val) {
      _core.dogma.raise("%s must not be same as %s.", (0, _core.fmt)(this.value), (0, _core.fmt)(val));
    }
  }
  return this;
};

Self.prototype.lt = function (val) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("val", val, null);

  {
    if ((0, _core.typename)(this.value) != (0, _core.typename)(val) || this.value >= val) {
      _core.dogma.raise("'%s' must be less than '%s'.", this.value, val);
    }
  }
  return this;
};

Self.prototype.le = function (val) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("val", val, null);

  {
    if ((0, _core.typename)(this.value) != (0, _core.typename)(val) || this.value > val) {
      _core.dogma.raise("'%s' must be less than or equal to '%s'.", this.value, val);
    }
  }
  return this;
};

Self.prototype.gt = function (val) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("val", val, null);

  {
    if ((0, _core.typename)(this.value) != (0, _core.typename)(val) || _core.dogma.includes(["bool"], (0, _core.typename)(this.value)) || this.value <= val) {
      _core.dogma.raise("'%s' must be greater than '%s'.", this.value, val);
    }
  }
  return this;
};

Self.prototype.ge = function (val) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("val", val, null);

  {
    if ((0, _core.typename)(this.value) != (0, _core.typename)(val) || _core.dogma.includes(["bool"], (0, _core.typename)(this.value)) || this.value < val) {
      _core.dogma.raise("'%s' must be greater than or equal to '%s'.", this.value, val);
    }
  }
  return this;
};

Self.prototype.between = function (val1, val2) {
  {
    if ((0, _core.typename)(this.value) != (0, _core.typename)(val1) || (0, _core.typename)(this.value) != (0, _core.typename)(val2) || this.value < val1 || this.value > val2) {
      _core.dogma.raise("'%s' must be between '%s' and '%s'.", this.value, val1, val2);
    }
  }
  return this;
};

Self.prototype.notBetween = function (val1, val2) {
  {
    if ((0, _core.typename)(this.value) == (0, _core.typename)(val1) && (0, _core.typename)(this.value) == (0, _core.typename)(val2) && this.value >= val1 && this.value <= val2) {
      _core.dogma.raise("'%s' must not be between '%s' and '%s'.", this.value, val1, val2);
    }
  }
  return this;
};

Self.prototype.includes = function (vals) {
  vals = (0, _core.list)(vals);
  {
    if ((0, _core.len)(vals) == 0) {
      _core.dogma.raise("'%s' must include something non-indicated.", this.value);
    }

    for (const val of vals) {
      if (_core.dogma.is(this.value, _core.list) && !_core.dogma.includes(["bool", "num", "text", "nil"], (0, _core.typename)(val))) {
        let exists = false;

        for (const item of this.value) {
          [exists] = _core.dogma.getArrayToUnpack(_core.dogma.peval(() => {
            return assert.deepEqual(item, val);
          }), 1);

          if (exists) {
            break;
          }
        }

        if (!exists) {
          _core.dogma.raise("'%s' must include '%s'.", this.value, val);
        }
      } else {
        {
          const [ok, res] = _core.dogma.peval(() => {
            return this.value.includes(val);
          });

          if (!ok || !res) {
            _core.dogma.raise("'%s' must include '%s'.", this.value, val);
          }
        }
      }
    }
  }
  return this;
};

Self.prototype.doesNotInclude = Self.prototype.notIncludes = function (vals) {
  vals = (0, _core.list)(vals);
  {
    for (const val of vals) {
      if (_core.dogma.is(this.value, _core.list) && !_core.dogma.includes(["bool", "num", "text", "nil"], (0, _core.typename)(val))) {
        let exists = false;

        for (const item of this.value) {
          [exists] = _core.dogma.getArrayToUnpack(_core.dogma.peval(() => {
            return assert.deepEqual(item, val);
          }), 1);

          if (exists) {
            _core.dogma.raise("'%s' must not include '%s'.", this.value, val);
          }
        }
      } else {
        {
          const [ok, res] = _core.dogma.peval(() => {
            return this.value.includes(val);
          });

          if (ok && res) {
            _core.dogma.raise("'%s' must not include '%s'.", this.value, val);
          }
        }
      }
    }
  }
  return this;
};

Self.prototype.has = function (mems) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("mems", mems, [_core.text, _core.list, _core.map]);

  {
    if (_core.dogma.isNot(mems, [_core.list, _core.map])) {
      mems = [mems];
    }

    if (_core.dogma.is(mems, _core.list)) {
      for (const mem of mems) {
        if (_core.dogma.getItem(this.value, mem) == null) {
          _core.dogma.raise("'%s' must have '%s'.", (0, _core.fmt)(this.value), mem);
        }
      }
    } else {
      for (const [mem, val] of Object.entries(mems)) {
        {
          if (!_core.dogma.getItem(_core.dogma.peval(() => {
            return assert.deepEqual(_core.dogma.getItem(this.value, mem), val);
          }), 0)) {
            _core.dogma.raise("%s must have '%s' to %s. Received: %s.", (0, _core.fmt)(this.value), mem, (0, _core.fmt)(val), _core.dogma.getItem(this.value, mem));
          }
        }
      }
    }
  }
  return this;
};

Self.prototype.forEach = function (check) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("check", check, [_core.map, _core.func]);

  {
    if (_core.dogma.is(check, _core.func)) {
      for (const item of this.value) {
        check(item);
      }
    } else {
      for (const item of this.value) {
        Self(item).has(check);
      }
    }
  }
  return this;
};

Self.prototype.doesNotHave = Self.prototype.notHas = function (mems) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("mems", mems, [_core.text, _core.list, _core.map]);

  {
    if (_core.dogma.isNot(mems, [_core.list, _core.map])) {
      mems = [mems];
    }

    if (_core.dogma.is(mems, _core.list)) {
      for (const mem of mems) {
        if (_core.dogma.getItem(this.value, mem) !== undefined) {
          _core.dogma.raise("%s must not have '%s'.", (0, _core.fmt)(this.value), mem);
        }
      }
    } else {
      for (const [mem, val] of Object.entries(mems)) {
        {
          if (_core.dogma.getItem(this.value, mem) == val) {
            _core.dogma.raise("%s must not have '%s' to '%s'. Received: '%s'.", (0, _core.fmt)(this.value), mem, val, _core.dogma.getItem(this.value, mem));
          }
        }
      }
    }
  }
  return this;
};

function similar(arr1, arr2) {
  let res = false;
  /* istanbul ignore next */

  _core.dogma.paramExpected("arr1", arr1, null);
  /* istanbul ignore next */


  _core.dogma.paramExpected("arr2", arr2, null);

  {
    if ((0, _core.len)(arr1) != (0, _core.len)(arr2)) {
      res = false;
    } else {
      if ((0, _core.len)(arr1) == 0) {
        res = true;
      } else {
        for (const i of arr1) {
          res = false;

          for (const j of arr2) {
            [res] = _core.dogma.getArrayToUnpack(_core.dogma.peval(() => {
              return assert.deepEqual(i, j);
            }), 1);

            if (res) {
              break;
            }
          }

          if (!res) {
            break;
          }
        }
      }
    }
  }
  return res;
}

Self.prototype.similarTo = function (arr) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("arr", arr, null);

  {
    if (!similar(this.value, arr)) {
      _core.dogma.raise("%s must be similar to %s.", (0, _core.fmt)(this.value), (0, _core.fmt)(arr));
    }
  }
  return this;
};

Self.prototype.notSimilarTo = function (arr) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("arr", arr, null);

  {
    if (similar(this.value, arr)) {
      _core.dogma.raise("%s must not be similar to %s.", (0, _core.fmt)(this.value), (0, _core.fmt)(arr));
    }
  }
  return this;
};

Self.prototype.isEmpty = function () {
  {
    if (_core.dogma.includes(["bool", "num"], (0, _core.typename)(this.value)) || (0, _core.len)(this.value) > 0) {
      _core.dogma.raise("value must be empty.");
    }
  }
  return this;
};

Self.prototype.isNotEmpty = function () {
  {
    if (_core.dogma.includes(["bool", "num"], (0, _core.typename)(this.value)) || (0, _core.len)(this.value) == 0) {
      _core.dogma.raise("value must not be empty.");
    }
  }
  return this;
};

Self.prototype.len = function (size) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("size", size, [_core.num, _core.list]);

  {
    if (_core.dogma.is(size, _core.list)) {
      size = (0, _core.len)(size);
    }

    let s;

    if (_core.dogma.includes(["bool", "num"], (0, _core.typename)(this.value)) || (s = (0, _core.len)(this.value)) != size) {
      _core.dogma.raise("%s length must be '%s'. Received: %s.", (0, _core.fmt)(this.value), size, s);
    }
  }
  return this;
};

Self.prototype.notLen = function (size) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("size", size, _core.num);

  {
    if (_core.dogma.includes(["bool", "num"], (0, _core.typename)(this.value)) || (0, _core.len)(this.value) == size) {
      _core.dogma.raise("%s length must not be '%s'.", (0, _core.fmt)(this.value), size);
    }
  }
  return this;
};

Self.prototype.like = function (pat) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("pat", pat, _core.text);

  {
    if (!RegExp(pat).test(this.value)) {
      _core.dogma.raise("'%s' must be like '%s'.", this.value, pat);
    }
  }
  return this;
};

Self.prototype.notLike = function (pat) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("pat", pat, _core.text);

  {
    if (RegExp(pat).test(this.value)) {
      _core.dogma.raise("'%s' must not be like '%s'.", this.value, pat);
    }
  }
  return this;
};

Self.prototype.startsWith = function (prefix) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("prefix", prefix, _core.text);

  {
    if (!(0, _core.text)(this.value).startsWith(prefix)) {
      _core.dogma.raise("'%s' must start with '%s'.", this.value, prefix);
    }
  }
  return this;
};

Self.prototype.doesNotStartWith = Self.prototype.notStartsWith = function (prefix) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("prefix", prefix, _core.text);

  {
    if ((0, _core.text)(this.value).startsWith(prefix)) {
      _core.dogma.raise("'%s' must not start with '%s'.", this.value, prefix);
    }
  }
  return this;
};

Self.prototype.endsWith = function (suffix) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("suffix", suffix, _core.text);

  {
    if (!(0, _core.text)(this.value).endsWith(suffix)) {
      _core.dogma.raise("'%s' must end with '%s'.", this.value, suffix);
    }
  }
  return this;
};

Self.prototype.doesNotEndWith = Self.prototype.notEndsWith = function (suffix) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("suffix", suffix, _core.text);

  {
    if ((0, _core.text)(this.value).endsWith(suffix)) {
      _core.dogma.raise("'%s' must not end with '%s'.", this.value, suffix);
    }
  }
  return this;
};