"use strict";

var _core = require("@dogmalang/core");

const ValueWrapper = _core.dogma.use(require("./ValueWrapper"));

function assert(...args) {
  {
    return ValueWrapper(...args);
  }
}

module.exports = exports = assert;

assert.plugin = plugin => {
  /* istanbul ignore next */
  _core.dogma.paramExpected("plugin", plugin, _core.map);

  {
    for (const [key, val] of Object.entries(plugin)) {
      {
        _core.dogma.setItem("=", assert, key, val);
      }
    }

    return assert;
  }
};