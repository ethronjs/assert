//imports
const {cat} = require("ethron");
const exec = require("@ethronjs/plugin.exec");
const fs = require("@ethronjs/plugin.fs");
const npm = require("@ethronjs/plugin.npm");
const eslint = require("@ethronjs/plugin.eslint");
const babel = require("@ethronjs/plugin.babel");

//Package name.
const pkg = require("./package.json").name;

//Who can publish the package on NPM.
const who = "ethronjs";

//catalog
cat.macro("lint", [
  [exec, "dogmac check src test"],
  [eslint, "."]
]).title("Lint source code");

cat.macro("trans-dogma", [
  [fs.rm, "./build"],
  [exec, "dogmac js -o build/src src"],
  [exec, "dogmac js -o build/test/unit test/unit"]
]).title("Transpile from Dogma to JS").hidden(true);

cat.macro("trans-js", [
  [fs.rm, "dist"],
  [babel, "build", `dist/${pkg}/`]
]).title("Transpile from JS to JS").hidden(true);

cat.macro("build", [
  cat.get("lint"),
  cat.get("trans-dogma"),
  cat.get("trans-js"),
  [fs.cp, "package.json", `dist/${pkg}/package.json`],
  [fs.cp, "README.md", `dist/${pkg}/README.md`],
]).title("Build package");

cat.macro("test", `./dist/${pkg}/test/unit`).title("Unit testing");

cat.call("pub", npm.publish, {
  who,
  access: "public",
  path: `dist/${pkg}`
}).title("Publish on NPM");

cat.macro("dflt", [
  cat.get("build"),
  cat.get("test")
]).title("Build and test");
